import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Body from './Components/Body/body';
import { BrowserRouter } from 'react-router-dom'

class App extends React.Component {


  render() {
    return (
      <BrowserRouter>
        <div className="App" >
          <Body />
        </div>
      </BrowserRouter>
    );
  }
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
