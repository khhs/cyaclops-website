import React from 'react';
import './weather.css';

class Weather extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      weatherData: null,
      geoData: null
    };
    this.getLocation();
  }

  getWeather(){
    fetch("http://cyaclops.com/weather.php?city="+this.state.geoData.city)
    .then(res => res.json())
    .then((result) => {
        //console.log(result);
        this.setState({
          isLoaded: true,
          weatherData: result
        });
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }

  getLocation() {
    fetch("http://ip-api.com/json")
    .then(res => res.json())
    .then((result) => {
        //console.log(result);
        this.setState({
          geoData: result
        });
        this.getWeather();
      },
      (error) => {});
  }

  componentDidMount() {
  }

  render() {
    const { error, isLoaded, weatherData, geoData } = this.state;
    if (error) {
      return (
        <div>
          error
        </div>
      );
    }
    else if(!isLoaded){
      return <div>Loading...</div>;
    }
    else {
      return (
        <div id="weather-container">
        <div id="weather-wrapper">
          <div id="weather-icon-degrees">
            <div><img alt="" src={ "./Images/" + weatherData.query.results.channel.item.condition.code  + ".png"} /></div>
            <div>{Math.round(weatherData.query.results.channel.item.condition.temp)} °C</div>
          </div>
          <br />
          <div id="weather-city">
            {/*weatherData.query.results.channel.item.condition.text*/}
            {geoData.city}
          </div>
        </div>
        </div>
      );
    }
  }
}

export default Weather;
