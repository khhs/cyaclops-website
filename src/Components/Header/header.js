import React from 'react';
import './header.css';
import HeaderProfile from './Profile/profile-header';
import { Route, Link } from "react-router-dom";
import Home from '../Pages/Home/home';
import About from '../Pages/About/about';

class Header extends React.Component {
  render() {
    return (
      <div id="header-container">
        <div id="heading">
          <div id="logo">
            Logo
          </div>
          <div id="header-title">
            CyaClopS TV
          </div>
          <div id="profile-container">
            <HeaderProfile />
          </div>
        </div>
        <div id="navigation-container">
          <div className="header-empty-space"></div>
          <nav>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/about">Home</Link></li>
              <li><Link to="/">Home</Link></li>
            </ul>
          </nav>
          <div className="header-empty-space"></div>
        </div>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
      </div>
    );
  }
}

export default Header;
