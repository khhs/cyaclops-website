import React from 'react';
import './home.css';
import Weather from '../../Weather/weather.js';
import TwitchEmbed from '../../TwitchEmbed/twitchEmbed.js';

class Home extends React.Component {

  render() {
    return (
      <div id="home-container">
      <div className="empty-space"></div>
      <aside id="left-aside">
        This is the left aside;
      </aside>
      <div id="main">
         <TwitchEmbed />
      </div>
      <aside id="right-aside">
        <Weather />
      </aside>
      <div className="empty-space"></div>
      </div>
    );
  }
}

export default Home;
