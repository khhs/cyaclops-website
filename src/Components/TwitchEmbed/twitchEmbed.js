import React from 'react';

const EMBED_URL = 'http://player.twitch.tv/js/embed/v1.js';

class TwitchEmbed extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      userOnline: null,
      streamSource: "http://player.twitch.tv/?video=241499067&autoplay=true&mute=false",
      streamStarted: null,
      embed: null
    }
  }

  componentDidMount(){
    this.getTwitchOnlineStatus();
    const script = document.createElement('script');
    script.setAttribute( 'src', EMBED_URL );
    script.addEventListener('load', () => {
      this.setState({embed: new window.Twitch.Player("twitch-embed",{
        width: "640px",
        height: "360px",
        video: "241499067",
        channel: "cyaclops"
      })});
      this.state.embed.setVolume(0.5);
      this.state.embed.setMuted(false);
      //this.state.embed.seek(0);
      /*this.state.embed.addEventListener(window.Twitch.Player.ONLINE, (() => {
        this.online(this.state.embed);
      }));*/
      this.state.embed.addEventListener(window.Twitch.Player.OFFLINE, (() => {
        this.offline(this.state.embed);
      }));
    });
    document.body.appendChild(script);
  }

  online(player){
    console.log("ONLINE");
    player.setChannel("CyaClopS");
  }

  offline(player){
    console.log("OFFLINE");
      if(player != null){
        player.setVideo("v241499067");
        player.pause();
        return;
      }
      setTimeout(() => {
        this.offline(player);
      },100);
  }

  getTwitchOnlineStatus(){
    fetch('http://cyaclops.com/twitch.php')
    .then(res=>res.json())
    .then((res) => {
      if(res.data == ""){
        this.setState({userOnline: false});
      }
      else{
        if(!this.state.userOnline){
          this.online(this.state.embed);
        }
        if(this.state.userOnline && this.state.streamStarted != res.data[0].started_at){
          this.online(this.state.embed);
        }
        this.setState({userOnline: true, streamStarted: res.data[0].started_at});
      }
      console.log("ok");
      setTimeout(() => {
        this.getTwitchOnlineStatus();
      }, 1000 * 5);
    });
  }

  render() {
      return (
        <div id="twitch-container">
          <div id="twitch-embed"></div>
        </div>
      );
  }
}

export default TwitchEmbed;
