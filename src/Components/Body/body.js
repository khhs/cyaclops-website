import React from 'react';
import './body.css';
import Header from '../Header/header';

class Body extends React.Component {
  render() {
    return (
      <div>
        <Header />
      </div>
    );
  }
}

export default Body;
